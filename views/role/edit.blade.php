@php($role = $role ?? NULL)
@extends('layouts.app')

@section('content')

    <div class="container">

        <h1>{{ $role ? 'Edit' : 'Create' }} Role</h1>

        <form action="{{ $role ? route('mvcommerce.role.update', $role) : route('mvcommerce.role.store') }}"
              method="post">

            @csrf
            @method( $role ? 'PUT' : 'POST' )

            <div class="card mb-3">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="role-name" class="col-form-label col-sm-3 col-md-2">Role Name</label>
                        <div class="col-sm-9 col-md-10">
                            <input type="text"
                                   id="role-name"
                                   class="form-control"
                                   value="{{ request('name') ?? old('name') ?? $role->name ?? '' }}"
                                   name="name"
                                   autofocus
                                   placeholder="Role Name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="role-description" class="col-form-label col-sm-3 col-md-2">Description</label>
                        <div class="col-sm-9 col-md-10">
                            <input type="text"
                                   id="role-description"
                                   class="form-control"
                                   placeholder="Describe the usage of the role."
                                   value="{{ request('description') ?? old('description') ?? $role->description ?? '' }}"
                                   name="description">
                        </div>
                    </div>
                </div>
            </div>

            <div class="card mb-3">
                <div class="card-header">
                    Permissions ::
                    <button type="button" class="btn btn-link" onclick="permissionSelectAll()">Select All</button>
                    <button type="button" class="btn btn-link" onclick="permissionSelectNone()">Select None</button>
                </div>
                <div class="card-body">

                    <div class="form-group">
                        <input type="text"
                               class="form-control"
                               placeholder="Search..."
                               id="input-search"
                               onkeyup="permissionSearch(this)">
                    </div>

                    @foreach($permissions as $permission)
                        <div class="permission-item" data-permission="{{ $permission->name }}">
                            <div class="custom-control custom-checkbox my-1">
                                <input type="checkbox"
                                       class="custom-control-input"
                                       name="permissions[]"
                                       value="{{ $permission->id }}"
                                       {{ $role->permissions->contains('id', $permission->id) ? 'checked' : '' }}
                                       id="perm-{{ $permission->id }}">
                                <label class="custom-control-label" for="perm-{{ $permission->id }}">
                                    {{ $permission->name }}
                                    <span class="text-muted">&mdash; {{ $permission->description }}</span>
                                </label>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save Role</button>
            </div>

        </form>

    </div>


    <script>
        var permissions = document.getElementsByClassName('permission-item');
        var permission_search_last_value = '';

        function permissionSelectAll(){
            var inputs = document.querySelectorAll('.permission-item input');
            console.log(inputs);
            for(var i in inputs){
                if(!inputs.hasOwnProperty(i)) continue;
                inputs[i].checked = true;
                console.log(inputs[i].checked);
            }
        }

        function permissionSelectNone(){
            var inputs = document.querySelectorAll('.permission-item input');
            for(var i in inputs){
                if(!inputs.hasOwnProperty(i)) continue;
                inputs[i].checked = false;
            }
        }

        function permissionSelectInverse(){
            var inputs = document.querySelectorAll('.permission-item input');
            for(var i in inputs){
                if(!inputs.hasOwnProperty(i)) continue;
                inputs[i].checked = !inputs[i].checked;
            }
        }

        function permissionSearch(input) {
            var i;
            var search_str = input.value;

            search_str = search_str.replace('.', '\\.');
            search_str = search_str.replace(/[ ]+/, '.*');
            console.log(search_str);

            if(permission_search_last_value === search_str){
                for(i in permissions){
                    if(!permissions.hasOwnProperty(i)) continue;
                    permissions[i].classList.remove('d-none');
                }
                return;
            }

            for(i in permissions){
                if(!permissions.hasOwnProperty(i)) continue;
                var permission = permissions[i];
                var p_value = permission.getAttribute('data-permission');
                if( p_value.match(new RegExp(search_str)) ){
                    permission.classList.remove('d-none');
                }else{
                    permission.classList.add('d-none');
                }
            }
        }
    </script>

@endsection
