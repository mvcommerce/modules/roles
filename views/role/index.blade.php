@extends('layouts.app')

@section('content')

    <div class="container">

        <h1>Manage Roles</h1>

        <div class="card mb-3">
            <div class="card-body p-0">
                <form action="{{ route('mvcommerce.role.index') }}" method="get">
                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th class="border-top-0" width="300">Role</th>
                                <th class="border-top-0" >Permissions</th>
                                <th class="border-top-0" width="120">
                                    <a href="{{ route('mvcommerce.role.create') }}" class="btn btn-success btn-block">Add</a>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="2">
                                    <input type="text"
                                           class="form-control"
                                           name="q"
                                           value="{{ request('q') ?? old('q') }}"
                                           placeholder="Search...">
                                </th>
                                <th>
                                    <button class="btn btn-primary btn-block">Filter</button>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($roles as $role)
                                <tr>
                                    <td>
                                        {{ $role->name }}
                                        <div class="small text-muted">{{ $role->description }}</div>
                                    </td>
                                    <td>{{ $role->permissions->implode('name', ', ') }}</td>
                                    <td><a href="{{ route('mvcommerce.role.edit', $role) }}"
                                           class="btn btn-primary">Edit</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    @if(!count($roles))
                        <div class="alert alert-danger">Sorry, we couldn't find any Roles.</div>
                    @endif

                    {{ $roles->links() }}

                </form>
            </div>
        </div>

    </div>

@endsection
