<?php

namespace MVCommerceModules\Roles\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;

class BladeTemplatesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(){

    }


    /**
     * Register the application services.
     *
     * @return void
     * @throws \Throwable
     */
    public function register(){
        $this->app->afterResolving('blade.compiler', function (BladeCompiler $bladeCompiler) {
            $this->registerBladeExtensions($bladeCompiler);
        });
    }


    protected function registerBladeExtensions(BladeCompiler $bladeCompiler){

        $bladeCompiler->directive('role', function ($arguments) {
            return "<?php if(auth()->user()->hasRoles({$arguments})): ?>";
        });

        $bladeCompiler->directive('elserole', function ($arguments) {
            return "<?php elseif(auth()->user()->hasRoles({$arguments})): ?>";
        });

        $bladeCompiler->directive('endrole', function () {
            return '<?php endif; ?>';
        });

        $bladeCompiler->directive('endroles', function () {
            return '<?php endif; ?>';
        });

        $bladeCompiler->directive('hasrole', function ($arguments) {
            return "<?php if(auth()->user()->hasRoles({$arguments})): ?>";
        });

        $bladeCompiler->directive('hasroles', function ($arguments) {
            return "<?php if(auth()->user()->hasRoles({$arguments})): ?>";
        });

        $bladeCompiler->directive('elsehasrole', function ($arguments) {
            return "<?php elseif(auth()->user()->hasRoles({$arguments})): ?>";
        });

        $bladeCompiler->directive('elsehasroles', function ($arguments) {
            return "<?php elseif(auth()->user()->hasRoles({$arguments})): ?>";
        });

        $bladeCompiler->directive('endhasrole', function () {
            return '<?php endif; ?>';
        });

        $bladeCompiler->directive('endhasroles', function () {
            return '<?php endif; ?>';
        });

        $bladeCompiler->directive('hasallroles', function ($arguments) {
            return "<?php if(auth()->user()->hasAllRoles({$arguments})): ?>";
        });

        $bladeCompiler->directive('elsehasallroles', function ($arguments) {
            return "<?php elseif(auth()->user()->hasAllRoles({$arguments})): ?>";
        });

        $bladeCompiler->directive('endhasallroles', function () {
            return '<?php endif; ?>';
        });

        $bladeCompiler->directive('unlessrole', function ($arguments) {
            return "<?php if(!auth()->user()->hasRoles({$arguments})): ?>";
        });

        $bladeCompiler->directive('unlessroles', function ($arguments) {
            return "<?php if(!auth()->user()->hasRoles({$arguments})): ?>";
        });

        $bladeCompiler->directive('unlessallroles', function ($arguments) {
            return "<?php if(!auth()->user()->hasAllRoles({$arguments})): ?>";
        });

        $bladeCompiler->directive('endunlessrole', function () {
            return '<?php endif; ?>';
        });

        $bladeCompiler->directive('endunlessroles', function () {
            return '<?php endif; ?>';
        });

    }

}

