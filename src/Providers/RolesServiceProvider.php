<?php

namespace MVCommerceModules\Roles\Providers;


use Illuminate\Contracts\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Routing\Route;
use Illuminate\Support\ServiceProvider;
use MVCommerceModules\Roles\Controllers\RoleController;

class RolesServiceProvider extends ServiceProvider
{


    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(){

        $this->loadMigrationsFrom(__DIR__ . '/../../migrations');
        $this->loadViewsFrom(__DIR__ . '/../../views', 'mvcommerce_roles');
        $this->loadRoutesFrom(__DIR__ . '/../../routes.php');

        $this->publishes([
            __DIR__ . '/../../migrations' => database_path('migrations'),
        ], ['mvcommerce', 'mvcommerce_roles', 'mvcommerce_roles_migrations', 'migrations']);

        $this->publishes([
            __DIR__.'/../../config/roles.php' => config_path('mvcommerce/roles.php'),
        ], ['mvcommerce', 'mvcommerce_roles', 'mvcommerce_roles_config', 'config']);

        $this->publishes([
            __DIR__.'/../../views' => resource_path('views/vendor/mvcommerce_roles'),
        ], ['mvcommerce', 'mvcommerce_roles', 'mvcommerce_roles_views', 'views']);




        $this->app->register(BladeTemplatesServiceProvider::class);


        // Registers the Permissions in the Gate so, we can use "can()" method to check permissions.
        $this->registerPermissions();
        // $this->registerMacroHelpers();


    }


    /**
     * Register the application services.
     *
     * @return void
     * @throws \Throwable
     */
    public function register()
    {

        $this->mergeConfigFrom(
            __DIR__.'/../../config/roles.php', 'mvcommerce.roles'
        );

    }


    public function registerMacroHelpers(){

        Route::macro('permission', function ($permissions = []) {

            if (! is_array($permissions)) {
                $permissions = [$permissions];
            }

            $permissions = implode('|', $permissions);
            $this->middleware("permission:$permissions");

            return $this;
        });

    }


    public function registerPermissions(){

        app(Gate::class)->before(function (Authorizable $user, string $ability) {
            if (method_exists($user, 'hasPermission')) {
                return $user->hasPermission($ability) ?: null;
            }
        });

        return true;

    }


}
