<?php

namespace MVCommerceModules\Roles\Traits;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use MVCommerceModules\Roles\Models\Permission;
use MVCommerceModules\Roles\Models\Role;


/**
 * Trait HasRoles
 * @package MVCommerce\Roles\Traits
 *
 * @property Collection $roles
 * @property Collection $permissions
 */
trait HasRoles
{

    protected $_roles, $_permissions;


    public static function bootHasRoles(){

        static::deleting(function (Model $model) {

            // If supports force deleting, then detach on force deleting.
            // Otherwise, delete on general delete event.

            if(method_exists($model, 'isForceDeleting')){
                if($model->isForceDeleting()){
                    $model->roles()->detach();
                }
            }else{
                $model->roles()->detach();
            }

        });


        static::created(function (Model $model) {
            $defaultRole = $model->defaultRole();
            if($defaultRole){
                $model->addRoles($defaultRole);
            }
        });

    }


    public function defaultRole(){
        return Role::findByName(config('mvcommerce.role.default_role', 'Super Admin'));
    }


    /**
     * Get the cache key for permission of that particular model.
     *
     * @param string $type
     * @return \Illuminate\Config\Repository|mixed|string
     * @throws \Throwable
     */
    public function getRoleAndPermissionCacheKey(string $type){

        throw_unless($this->getKey(), 'Model has to saved first.');

        $cache_key = config('mvcommerce.roles.cache.perfix', 'mvcommerce.roles.cache');
        $cache_key .= '.' . $this->getKey() . '.' . $type;
        return $cache_key;
    }


    public function getRoleAndPermissionCacheDriver(){
        $driver = config('mvcommerce.roles.cache.driver');
        return $driver ?? config('cache.default');
    }


    public function getRoleAndPermissionCacheTimeout(){
        return config('mvcommerce.roles.cache.timeout', 300);
    }


    /**
     * @throws \Exception
     */
    public function clearRoleAndPermissionCaches(){
        foreach (['roles', 'permissions'] as $type){

            $cache_key = $this->getRoleAndPermissionCacheKey($type);

            \cache()
                ->driver($this->getRoleAndPermissionCacheDriver())
                ->forget($cache_key);

        }

        $this->_roles = NULL;
        $this->_permissions = NULL;

    }


    /**
     * @param string $type
     * @param callable $callable
     * @return mixed
     * @throws \Throwable
     */
    protected function roleAndPermissionCache(string $type, callable $callable){
        $cache_key = $this->getRoleAndPermissionCacheKey($type);

        return \cache()
            ->driver($this->getRoleAndPermissionCacheDriver())
            ->remember($cache_key, $this->getRoleAndPermissionCacheTimeout(), $callable);

    }


    /**
     * @return Collection
     * @throws \Throwable
     */
    protected function _cacheRoles(){
        return $this->roleAndPermissionCache('roles', function(){
            return $this->_roles = $this->roles()->get();
        });
    }


    /**
     * @return Collection
     * @throws \Throwable
     */
    protected function _cacheRolesIfNotCached(){
        return $this->_roles = $this->_roles ?? $this->_cacheRoles();
    }


    protected function _cachePermissions(){
        return $this->roleAndPermissionCache('permissions', function(){
            return $this->permissions();
        });
    }

    protected function _cachePermissionsIfNotCached(){
        return $this->_permissions = $this->_permissions ?? $this->_cachePermissions();
    }



    // Relations ------------------------------------------

    /**
     * @return BelongsToMany
     */
    public function roles(){
        return $this->morphToMany(Role::class, 'user', 'role_user', 'user_id');
    }



    public function getRolesAttribute(){
        return $this->_cacheRolesIfNotCached();
    }


    public function permissions(){
        return $this->loadMissing('roles.permissions')
            ->roles->flatMap(function($role){
                return $role->permissions;
            });
    }


    public function getPermissionsAttribute(){
        return $this->_cachePermissionsIfNotCached();
    }




    /**
     * Matches with 1 or at least one of many permissions.
     *
     * @param string|array $permissions
     * @return bool
     */
    public function hasPermission(...$permissions){

        $permissions = Arr::wrap( Arr::flatten($permissions) );

        $column = 'name';
        if( is_integer( reset($permissions) ) ){
            $column = 'id';
        }elseif (reset($permissions) instanceof Role){
            $column = 'id';
            $permissions = array_map(function($permissions){
                return $permissions->id;
            }, $permissions);
        }

        return $this->permissions->whereIn($column, $permissions)->count() > 0;
    }


    /**
     * Alias of hasPermission($permission) method.
     *
     * @param string|array $permissions
     * @return bool
     */
    public function hasPermissions(...$permissions){
        return $this->hasPermission($permissions);
    }


    /**
     * Should match with all mentioned permissions
     *
     * @param array $permissions
     * @return bool
     */
    public function hasAllPermissions(...$permissions){
        $permissions = Arr::wrap( Arr::flatten($permissions) );
        return $this->permissions->whereIn('name', $permissions) === count($permissions);
    }


    /**
     * Matches with 1 or at least one of many role.
     *
     * @param string|array $roles
     * @return bool
     */
    public function hasRole(...$roles){

        $roles = Arr::wrap( Arr::flatten($roles) );

        $column = 'name';
        if( is_integer( reset($roles) ) ){
            $column = 'id';
        }elseif (reset($roles) instanceof Role){
            $column = 'id';
            $roles = array_map(function($role){
                return $role->id;
            }, $roles);
        }

        return $this->roles->whereIn($column, $roles)->count() > 0;
    }


    /**
     * Alias of hasRole($role) method.
     *
     * @param string|array $roles
     * @return bool
     */
    public function hasRoles(...$roles){
        return $this->hasRole($roles);
    }


    /**
     * Should match with all mentioned roles
     *
     * @param array $roles
     * @return bool
     */
    public function hasAllRoles(...$roles){

        $roles = Arr::wrap( Arr::flatten($roles) );

        $column = 'name';
        if( is_integer( reset($roles) ) ){
            $column = 'id';
        }

        return $this->roles->whereIn($column, $roles)->count() === count($roles);
    }


    /**
     * @param $roles
     * @param bool $remove_others
     * @throws \Exception
     */
    public function addRoles($roles, $remove_others = false){
        $this->roles()->sync($roles, $remove_others);
        $this->clearRoleAndPermissionCaches();
    }


    /**
     * @param $roles
     * @throws \Exception
     */
    public function addRolesOnly($roles){
        $this->addRoles($roles, true);
        $this->clearRoleAndPermissionCaches();
    }


    /**
     * @param $roles
     * @throws \Exception
     */
    public function removeRoles($roles){
        $this->roles()->detach($roles);
        $this->clearRoleAndPermissionCaches();
    }

}
