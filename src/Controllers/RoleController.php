<?php


namespace MVCommerceModules\Roles\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MVCommerceModules\Roles\Models\Permission;
use MVCommerceModules\Roles\Models\Role;

class RoleController extends Controller
{


    protected $required_permission;

    public function __construct(){

        $this->required_permission = config('roles.manage_roles_permission', 'manage_roles_and_permissions');

    }


    public function authorizeRequiredPermission(){
        $this->authorize($this->required_permission);
    }

    /**
     * Get All Roles.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){

        $this->authorizeRequiredPermission();

        $query = Role::query()
            ->with('permissions')
            ->filters(request()->all());

        $roles = $query->paginate()
            ->appends(request()->all());

        return view('mvcommerce_roles::role.index', compact('roles'));

    }


    /**
     * Create new Role
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(){

        $this->authorizeRequiredPermission();

        $permissions = Permission::all();

        return view('mvcommerce_roles::role.edit', compact('permissions'));

    }


    public function store(Request $request){

        $this->authorizeRequiredPermission();

        $data = $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'description' => 'nullable',
            'permissions.*' => 'nullable|exists:permissions,id',
        ]);

        $role = new Role($data);
        $role->saveOrFail();

        $role->permit($data['permissions'] ?? [], true);

        return redirect()->route('mvcommerce.role.edit', $role);
    }


    /**
     * Edit a Role
     * @param Request $request
     * @param $role
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($role){

        $this->authorizeRequiredPermission();

        /** @var Role $role */
        $role = Role::findOrFail($role);
        $permissions = Permission::orderBy('name')->get();

        return view('mvcommerce_roles::role.edit', compact(
            'role', 'permissions'
        ));

    }


    /**
     * Updates a Role and assigns the permission.
     *
     * @param Request $request
     * @param $role
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $role){

        $this->authorizeRequiredPermission();

        /** @var Role $role */
        $role = Role::findOrFail($role);

        $data = $this->validate($request, [
            'name' => 'required|unique:roles,name,' . $role->getKey() . ',' . $role->getKeyName(),
            'description' => 'nullable',
            'permissions.*' => 'nullable|exists:permissions,id',
        ]);

        $role->fill($data);
        $role->saveOrFail();
        $role->permit($data['permissions'] ?? [], true);

        return back();

    }

}
