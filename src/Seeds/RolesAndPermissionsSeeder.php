<?php

namespace MVCommerceModules\Roles\Seeds;

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use MVCommerceModules\Roles\Models\Permission;
use MVCommerceModules\Roles\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{

    public function run(){

        DB::transaction(function(){

            $role = new Role([
                'name' => 'Super Admin',
                'description' => 'A super-admin can manage everything.'
            ]);

            $permissions = [
                'manage_user_roles' => 'Can manage roles of other users.',
                'manage_roles_and_permissions' => 'Can manage roles & permissions of other users.',
            ];

            $role->saveOrFail();


            $permits = [];
            foreach ($permissions as $permission => $description){

                $permission = new Permission([
                    'name' => $permission,
                    'description' => $description,
                ]);

                $permission->save();
                $permits[] = $permission->id;

            }

            $role->permit($permits);

            // Set the first user as super-admin.
            /** @var User $user */
            $user = User::first();
            if($user){
                $user->addRoles($user);
            }

        });


    }

}
