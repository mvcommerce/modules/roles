<?php

namespace MVCommerceModules\Roles\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

/**
 * @property int $id
 * @property Permission[]|Collection $permissions
 */
class Role extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'name', 'description'
    ];


    // Relations ------------------------------------------

    public function permissions(){
        return $this->belongsToMany(Permission::class);
    }


    public function users($user_type){
        return $this->morphedByMany($user_type, 'user', 'role_user');
    }



    // Scopes -----------------------------------------------


    public function scopeFilters(Builder $query, $filters = []){

        if($filters['q'] ?? false){
            $char = '\\';
            $q = $filters['q'];
            $q = str_replace(
                [$char, '%', '_'],
                [$char.$char, $char.'%', $char.'_'],
                $q
            );
            $q = str_replace( ' ', '%', $q );
            $q = "%$q%";
            $query->where(function(Builder $query) use ($q){
                $query->where('name', 'LIKE', $q);
                $query->orWhere('description', 'LIKE', $q);
            });
        }

    }


    // Methods --------------------------------------------


    /**
     * Find a role by name.
     *
     * @param string $name
     * @return mixed
     */
    public static function findByName(string $name){
        return static::query()->where('name', $name)->first();
    }



    /**
     * @param string|array|Permission|Permission[] $permissions
     * @param bool $remove_others
     */
    public function permit($permissions, $remove_others = false){


        if( is_string($permissions) ){
            $permissions = Arr::wrap($permissions);
        }

        if($permissions instanceof Permission){
            $permissions = collect([ $permissions ]);
        }

        $this->permissions()->sync($permissions, $remove_others);

    }


    /**
     * @param $permissions
     */
    public function permitOnly($permissions){
        $this->permit($permissions, true);
    }


    /**
     * @param string|array|Permission|Permission[] $permissions
     */
    public function revoke($permissions){

        if( is_string($permissions) ){
            $permissions = Arr::wrap($permissions);
        }

        if($permissions instanceof Permission){
            $permissions = Arr::wrap($permissions);
        }

        if( is_array($permissions) ){

            $first = reset($permissions);

            if(is_string($first)){
                $permissions = Permission::query()
                    ->whereIn('name', $permissions)
                    ->pluck('id');
            }

        }

        $this->permissions()->detach($permissions);

    }

}
