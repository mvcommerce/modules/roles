<?php

namespace MVCommerceModules\Roles\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

/**
 * @property int $id
 * @property Role[]|Collection $roles
 */
class Permission extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'description'
    ];


    // Relations ------------------------------------------

    public function roles(){
        return $this->belongsToMany(Role::class);
    }



    // Methods --------------------------------------------


    /**
     * Add the permission to one on many Roles.
     *
     * @param string|array|Permission|Permission[] $role
     * @param bool $remove_others
     */
    public function addToRole($role, $remove_others = false){

        $this->roles()->sync($role, $remove_others);

    }


    /**
     * Alias of addToRole($role)
     * @param $roles
     * @param $remove_others
     */
    public function addToRoles($roles, $remove_others){
        $this->addToRole($roles, $remove_others);
    }


    /**
     * @param $role
     */
    public function removeFromRole($role){

        $this->roles()->detach($role);

    }


    /**
     * Alias of removeFromRole($role)
     * @param $roles
     */
    public function removeFromRoles($roles){
        $this->removeFromRole($roles);
    }

}
