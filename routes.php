<?php

use Illuminate\Support\Facades\Route;



$route_prefix = config('mvcommerce.roles.route_prefix', '/');


Route::namespace('MVCommerceModules\\Roles\\Controllers')
    ->prefix($route_prefix)
    ->middleware(['web', 'auth'])
    ->group(function(){

        Route::resource('role', 'RoleController')
            ->names([
                'index' => 'mvcommerce.role.index',
                'show' => 'mvcommerce.role.show',
                'create' => 'mvcommerce.role.create',
                'store' => 'mvcommerce.role.store',
                'edit' => 'mvcommerce.role.edit',
                'update' => 'mvcommerce.role.update',
                'destroy' => 'mvcommerce.role.destroy',
            ]);

    });
