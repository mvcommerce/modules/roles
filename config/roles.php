<?php

return [

    // Specify caching properties.

    'cache' => [
        'perfix' => 'mvcommerce.roles.cache',
        'driver' => NULL, // The cache driver. Set NULL to use default driver.
        'timeout' => 300, // (5 mins) - Timeout in minutes.
    ],


    // The default role that will automatically be assigned to users.
    'default_role' => 'Super Admin',


    // The Route prefix used to generate the administrative url.
    'route_prefix' => '/'

];
